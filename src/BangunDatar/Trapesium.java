package BangunDatar;

import java.util.Scanner;

public class Trapesium {
    private static Scanner input = new Scanner(System.in);

    private static double alasA;
    private static double alasB;
    private static double sisiA;
    private static double sisiB;
    private static double tinggi;

    public static void Luas() {
        System.out.print("Masukan Alas A Trapesium: ");
        alasA = input.nextDouble();

        System.out.print("Masukan Alas B Trapesium: ");
        alasB = input.nextDouble();

        System.out.print("Masukan Tinggi Trapesium: ");
        tinggi = input.nextDouble();

        double luasTrapesium = 0.5 * (alasA + alasB) * tinggi;
        System.out.println("Luas Trapesium = " + luasTrapesium);
        System.out.println("=========================================");
    }

    public static void Keliling() {
        System.out.print("Masukan Alas A Trapesium: ");
        alasA = input.nextDouble();

        System.out.print("Masukan Alas B Trapesium: ");
        alasB = input.nextDouble();

        System.out.print("Masukan Sisi A Trapesium: ");
        sisiA = input.nextDouble();

        System.out.print("Masukan Sisi B Trapesium: ");
        sisiB = input.nextDouble();

        double kelilingTrapesium = alasA + alasB + sisiA + sisiB;
        System.out.println("Keliling Trapesium " + kelilingTrapesium);
        System.out.println("=========================================");
    }
}
