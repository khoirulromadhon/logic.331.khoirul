package BangunDatar;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihanOperasi;
        boolean flag = true;
        String answer;

        while (flag){
            System.out.println("=========================================");
            System.out.println("Pilih bangun datar");
            System.out.println("1. Persegi Panjang");
            System.out.println("2. Segitiga");
            System.out.println("3. Trapesium");
            System.out.println("4. Lingkaran");
            System.out.println("5. Persegi");
            System.out.print("Masukan Pilihan: ");
            int pilihanBentuk = input.nextInt();

            switch (pilihanBentuk){
                case 1:
                    System.out.println("=========================================");
                    System.out.println("************ Persegi Panjang ************");
                    System.out.println("Pilih Operasi");
                    System.out.println("1. Luas");
                    System.out.println("2. Keliling");
                    System.out.print("Pilihan Operasi = ");
                    pilihanOperasi = input.nextInt();

                    if (pilihanOperasi == 1){
                        PersegiPanjang.Luas();
                    }
                    else if (pilihanOperasi == 2) {
                        PersegiPanjang.Keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia !!!!!");
                    }
                    break;
                case 2:
                    System.out.println("=========================================");
                    System.out.println("*************** Segitiga ***************");
                    System.out.println("Pilih Operasi");
                    System.out.println("1. Luas");
                    System.out.println("2. Keliling");
                    System.out.print("Pilihan Operasi = ");
                    pilihanOperasi = input.nextInt();

                    if (pilihanOperasi == 1){
                        Segitiga.Luas();
                    }
                    else if (pilihanOperasi == 2) {
                        Segitiga.Keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia !!!!!");
                    }
                    break;
                case 3:
                    System.out.println("=========================================");
                    System.out.println("*************** Trapesium ***************");
                    System.out.println("Pilih Operasi");
                    System.out.println("1. Luas");
                    System.out.println("2. Keliling");
                    System.out.print("Pilihan Operasi = ");
                    pilihanOperasi = input.nextInt();

                    if (pilihanOperasi == 1){
                        Trapesium.Luas();
                    }
                    else if (pilihanOperasi == 2) {
                        Trapesium.Keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia !!!!!");
                    }
                    break;
                case 4:
                    System.out.println("=========================================");
                    System.out.println("*************** Lingkaran ***************");
                    System.out.println("Pilih Operasi");
                    System.out.println("1. Luas");
                    System.out.println("2. Keliling");
                    System.out.print("Pilihan Operasi = ");
                    pilihanOperasi = input.nextInt();

                    if (pilihanOperasi == 1){
                        Lingkaran.Luas();
                    }
                    else if (pilihanOperasi == 2) {
                        Lingkaran.Keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia !!!!!");
                    }
                    break;
                case 5:
                    System.out.println("=========================================");
                    System.out.println("**************** Persegi ****************");
                    System.out.println("Pilih Operasi");
                    System.out.println("1. Luas");
                    System.out.println("2. Keliling");
                    System.out.print("Pilihan Operasi = ");
                    pilihanOperasi = input.nextInt();

                    if (pilihanOperasi == 1){
                        Persegi.Luas();
                    }
                    else if (pilihanOperasi == 2) {
                        Persegi.Keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia !!!!!");
                    }
                    break;
                default:
                    System.out.println("Pilihan Tidak Tersedia !!!!!");
            }
            System.out.print("Apakah ingin mencoba lagi ? y/n  ");
            input.nextLine(); // Skipp
            answer = input.nextLine();

            if (!answer.equalsIgnoreCase("y")){
                flag = false;
            }
        }
    }
}