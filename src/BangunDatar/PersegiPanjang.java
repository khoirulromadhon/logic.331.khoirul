package BangunDatar;

import java.util.Scanner;

public class PersegiPanjang {
    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;

    public static void Luas() {
        System.out.print("Masukan Panjang Persegi: ");
        panjang = input.nextInt();

        System.out.print("Masukan Lebar Persegi: ");
        lebar = input.nextInt();

        int luas = panjang * lebar;
        System.out.println("Luas persegi panjang adalah = " + luas);
        System.out.println("=========================================");
    }

    public static void Keliling() {
        System.out.print("Masukan Panjang Persegi: ");
        panjang = input.nextInt();

        System.out.print("Masukan Lebar Persegi: ");
        lebar = input.nextInt();

        int keliling = (2 * panjang) + (2 * lebar);
        System.out.println("Keliling persegi Panjang adalah = " + keliling);
        System.out.println("=========================================");
    }
}
