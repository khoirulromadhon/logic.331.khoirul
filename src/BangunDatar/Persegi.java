package BangunDatar;

import java.util.Scanner;

public class Persegi {
    private static Scanner input = new Scanner(System.in);

    private static double sisi;

    public static void Luas() {
        System.out.print("Masukan Sisi Persegi: ");
        sisi = input.nextDouble();

        double luasPersegi = sisi * sisi;
        System.out.println("Luas Persegi = " + luasPersegi);
        System.out.println("=========================================");
    }

    public static void Keliling() {
        System.out.print("Masukan Sisi Persegi: ");
        sisi = input.nextDouble();

        double kelilingPersegi = 4 * sisi;
        System.out.println("Keliling Persegi = " + kelilingPersegi);
        System.out.println("=========================================");
    }
}
