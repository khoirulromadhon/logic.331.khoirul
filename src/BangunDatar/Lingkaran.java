package BangunDatar;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);

    private static double jariJari;
    private static double phi = 22/7;

    public static void Luas() {
        System.out.print("Masukan Jari - jari Lingkaran: ");
        jariJari = input.nextDouble();

        double luasLingkaran = phi * jariJari * jariJari;
        System.out.println("Luas Lingkaran = " + luasLingkaran);
        System.out.println("=========================================");
    }

    public static void Keliling() {
        System.out.print("Masukan Jari - jari Lingkaran: ");
        jariJari = input.nextDouble();

        double kelilingLingkaran = 2 * phi * jariJari;
        System.out.println("Keliling Lingkaran = " + kelilingLingkaran);
        System.out.println("=========================================");
    }
}
