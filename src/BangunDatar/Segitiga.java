package BangunDatar;

import java.util.Scanner;

public class Segitiga {
    private static Scanner input = new Scanner(System.in);
    private static double sisi;
    private static double alas;
    private static double tinggi;

    public static void Luas() {
        System.out.print("Masukan Alas Segitiga: ");
        alas = input.nextDouble();

        System.out.print("Masukan Tinggi Segitiga: ");
        tinggi = input.nextDouble();

        double luasSegitiga = 0.5 * alas * tinggi;
        System.out.println("Luas Segitiga = " + luasSegitiga);
        System.out.println("=========================================");
    }

    public static void Keliling() {
        System.out.print("Masukan Sisi Segitiga: ");
        sisi = input.nextDouble();

        double kelilingSegitiga = sisi + sisi + sisi;
        System.out.println("Keliling Segitiga = " + kelilingSegitiga);
        System.out.println("=========================================");
    }
}
