package ProblemSolving14;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int jumlahUang, jumlahBarang, harga;

        System.out.print("Uang Andi: ");
        jumlahUang = input.nextInt();

        System.out.print("Jumlah Barang: ");
        jumlahBarang = input.nextInt();
        input.nextLine();

        String[] wishlist = new String[jumlahBarang];
        String[][] splitWishlist = new String[2][jumlahBarang];
        String[] item = new String[2];

        for (int i = 0; i < jumlahBarang; i++) {
            System.out.print("Wishlist ke" + (i + 1) + ": ");
            wishlist[i] = input.nextLine();
        }

        for (int i = 0; i < jumlahBarang; i++) {
            item = wishlist[i].split(", ");
            splitWishlist[0][i] = item[0];
            splitWishlist[1][i] = item[1];
        }

        for (int i = 0; i < jumlahBarang; i++) {
            harga = Integer.parseInt(splitWishlist[1][i]);

            if (jumlahUang >= harga) {
                System.out.print(splitWishlist[0][i] + ", ");
                jumlahUang -= harga;
            }
        }
        System.out.println();
    }
}
