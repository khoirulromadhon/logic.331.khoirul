package ProblemSolving14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Soal03 {
    public static void Resolve(){
        HashMap<String, Integer> fruits = new HashMap<String, Integer>();
        Scanner input = new Scanner(System.in);
        String data;
        int helper, index = 0;

        System.out.print("Masukan data: ");
        data = input.nextLine();

        String[] dataSplit = data.split(", ");
        String[] item = new String[2];

        for (int i = 0; i < dataSplit.length; i++) {
            item = dataSplit[i].split(":");
            if (fruits.get(item[0]) == null){
                fruits.put(item[0], Integer.parseInt(item[1]));
            }
            else {
                helper = fruits.get(item[0]);
                fruits.put(item[0], Integer.parseInt(item[1]) + helper);
            }
        }

        String[] keySet = new String[fruits.size()];
        for (String k : fruits.keySet()) {
            keySet[index] = k;
            index++;
        }

        Arrays.sort(keySet);

        for (String key : keySet) {
            System.out.println(key + ": " + fruits.get(key));
        }
    }
}
