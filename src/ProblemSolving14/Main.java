package ProblemSolving14;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 14)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 14){
                System.out.println("Angka tidak tersedia !!!!!");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1:
                    Soal01.Resolve();
                    break;
                case 2:
                    Soal02.Resolve();
                    break;
                case 3:
                    Soal03.Resolve();
                    break;
                case 4:
                    Soal04.Resolve(); // pd
                    break;
                case 5:
                    Soal05.Resolve();
                    break;
                case 6:
                    Soal06.Resolve();
                    break;
                case 7:
                    Soal07.Resolve();
                    break;
                case 8:
                    Soal08.Resolve(); // pd
                    break;
                case 9:
                    Soal09.Resolve();
                case 10:
                    Soal10.Resolve(); // pd
                    break;
                case 11:
                    Soal11.Resolve(); // pd
                    break;
                case 12:
                    Soal12.Resolve();
                    break;
                case 13:
                    Soal13.Resolve(); // pd
                    break;
                case 14:

                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}