package ProblemSolving14;

import java.util.Scanner;

public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int energi = 0, posisi = 0;
        char jalan;
        String lintasan, langkah;

        System.out.println("Contoh lintasan: -----o-o-");
        System.out.println(" - adalah jalan");
        System.out.println("o adalah lubang");
        System.out.print("Masukan lintasan: ");
        lintasan = input.nextLine();

        System.out.println("Contoh langkah: wwwjj");
        System.out.println("w adalah walk / jalan");
        System.out.println("j adalah lompat");
        System.out.print("Masukan langkah: ");
        langkah = input.nextLine();

        char[] lintasans = lintasan.toCharArray();
        char[] langkahs = langkah.toCharArray();

        for (int i = 0; i < langkahs.length; i++) {
            if (energi < 0  ) {
                System.out.println("Jim kehabisan energi");
                break;
            }
            else {
                if (langkahs[i] == 'w') {
                    if (lintasans[posisi] == 'o'){
                        System.out.println("Jim Died");
                        break;
                    }
                    else {
                        energi++;
                        posisi++;
                    }
                }
                else if (langkahs[i] == 'j') {
                    if (lintasans[posisi] == 'o'){
                        System.out.println("Jim Died");
                        break;
                    }
                    else {
                        energi-=2;
                        posisi+=2;
                    }
                }
                else {
                    System.out.println("langkah hanya w & j");
                }
            }
        }

        System.out.println("Energi " + energi);
    }
}
