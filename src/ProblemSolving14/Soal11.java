package ProblemSolving14;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int transaksi, totalPoint = 0, transaksiA, transaksiB, pointA, pointB;

        System.out.print("Masukan nominal transaksi: ");
        transaksi = input.nextInt();

        if (transaksi < 10000){
            totalPoint = 0;
        }
        else if (10000 < transaksi && transaksi  <= 30000) {
            totalPoint = transaksi / 1000;
        }
        else {
            transaksiA = transaksi - 30000; // point lebih dari 30.000
            transaksiB = transaksi - transaksiA; // point kurang dari 30.000

            pointA = transaksiB / 1000; // hitung point kurang dari 30.000
            pointB = (transaksiA / 1000) * 2; // hitung point lebih dari 30.000

            totalPoint = pointA + pointB; // hitung total point
        }

        System.out.println(totalPoint + " point");
    }
}
