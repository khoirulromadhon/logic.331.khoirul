package ProblemSolving14;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String time, format;
        int hour, minute;

        System.out.print("Input: ");
        time = input.nextLine();

        if (time.length() == 5) {
            String[] times = time.split(":");

            hour = Integer.parseInt(times[0]);
            minute = Integer.parseInt(times[1]);

            if (hour > 12){
                System.out.println("Output: " + (hour - 12) + ":" + minute + " PM");
            }
            else {
                System.out.println("Output: " + hour + ":" + minute + " AM");
            }
        }
        else if (time.length() > 5) {
            String[] times = time.split(":");

            hour = Integer.parseInt(times[0]);
            minute = Integer.parseInt(times[1].substring(0, 2));
            format = times[1].substring(2, 4);

            if (format.equalsIgnoreCase("PM") || format.equalsIgnoreCase("P")){
                System.out.println("Output: " + (hour + 12) + ":" + minute);
            }
            else {
                System.out.println("Output: " + hour + ":" + minute);
            }
        }
        else {
            System.out.println("Format tidak tersedia !!!!!");
        }
    }
}
