package ProblemSolving14;

import java.util.Scanner;
public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int panjangDeret;

        System.out.print("Masukan banyaknya deret: ");
        panjangDeret = input.nextInt();

        int[] deret1 = new int[panjangDeret];
        int[] deret2 = new int[panjangDeret];
        int[] penjumlahan = new int[panjangDeret];

        System.out.print("Deret Pertama: ");
        for (int i = 0; i < panjangDeret; i++) {
            deret1[i] = (i * 3) - 1;
            System.out.print(deret1[i] + " ");
        }
        System.out.println();

        System.out.print("Deret Kedua: ");
        for (int i = 0; i < panjangDeret; i++) {
            deret2[i] = (i * -2) * 1;
            System.out.print(deret2[i] + " ");
        }
        System.out.println();

        System.out.print("Output: ");
        for (int i = 0; i < panjangDeret; i++) {
            penjumlahan[i] = deret1[i] + deret2[i];
            System.out.print(penjumlahan[i] + " ");
        }
        System.out.println();
    }
}
