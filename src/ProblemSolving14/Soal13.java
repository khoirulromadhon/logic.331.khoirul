package ProblemSolving14;

import java.util.Scanner;

public class Soal13 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String huruf;

        System.out.println("Jika anda memasukan huruf kapital, \nmaka huruf akan otomatis di konversi ke huruf kecil");
        System.out.print("Huruf: ");
        huruf = input.nextLine();

        char[] hurufs = huruf.toLowerCase().toCharArray();
        int[] angkas = new int[hurufs.length];
        String[] outputs = new String[hurufs.length];

        for (int i = 0; i < hurufs.length; i++) {
            System.out.print("Masukan angka untuk huruf " + hurufs[i] + " : ");
            angkas[i] = input.nextInt();
        }

        for (int i = 0; i < hurufs.length; i++) {
            if (angkas[i] == (hurufs[i] - 96)){
                outputs[i] = "true";
            }
            else {
                outputs[i] = "false";
            }
        }

        for (String output : outputs) {
            System.out.print(output + ", ");
        }
        System.out.println();
    }
}
