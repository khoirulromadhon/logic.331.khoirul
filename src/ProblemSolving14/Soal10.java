package ProblemSolving14;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String wadahAwal, wadahKonversi;
        double volumeAwal, volumeKonversi;
        boolean flag = true;

        while (flag){
            System.out.println("Masukan nama wadah");
            System.out.println("Contoh: Botol, Teko, Gelas, Cangkir");
            System.out.print("Wadah: ");
            wadahAwal = input.nextLine();

            System.out.print("Masukan volume: ");
            volumeAwal = input.nextDouble();

            System.out.println("Anda ingin menkonversi ke wadah apa ?");
            System.out.println("Contoh: Botol, Teko, Gelas, Cangkir");
            System.out.println("Wadah: ");
            input.nextLine();
            wadahKonversi = input.nextLine();

            // Botol
            if (wadahAwal.equalsIgnoreCase("Botol") && wadahKonversi.equalsIgnoreCase("Gelas")){
                volumeKonversi = volumeAwal * 2;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Botol") && wadahKonversi.equalsIgnoreCase("Teko")) {
                volumeKonversi = volumeAwal / 5;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Botol") && wadahKonversi.equalsIgnoreCase("Cangkir")) {
                volumeKonversi = volumeAwal * 5;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Botol") && wadahKonversi.equalsIgnoreCase("Botol")) {
                volumeKonversi = volumeAwal * 1;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            // Teko
            else if (wadahAwal.equalsIgnoreCase("Teko") && wadahKonversi.equalsIgnoreCase("Cangkir")) {
                volumeKonversi = volumeAwal * 25;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Teko") && wadahKonversi.equalsIgnoreCase("Botol")) {
                volumeKonversi = volumeAwal * 5;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Teko") && wadahKonversi.equalsIgnoreCase("Gelas")) {
                volumeKonversi = volumeAwal * 10;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Teko") && wadahKonversi.equalsIgnoreCase("Teko")) {
                volumeKonversi = volumeAwal * 1;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            // Gelas
            else if (wadahAwal.equalsIgnoreCase("Gelas") && wadahKonversi.equalsIgnoreCase("Cangkir")) {
                volumeKonversi = volumeAwal * 2.5;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Gelas") && wadahKonversi.equalsIgnoreCase("Gelas")) {
                volumeKonversi = volumeAwal * 1;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Gelas") && wadahKonversi.equalsIgnoreCase("Botol")) {
                volumeKonversi = volumeAwal / 2;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Gelas") && wadahKonversi.equalsIgnoreCase("Teko")) {
                volumeKonversi = volumeAwal / 10;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            // Cangkir
            else if (wadahAwal.equalsIgnoreCase("Cangkir") && wadahKonversi.equalsIgnoreCase("Cangkir")) {
                volumeKonversi = volumeAwal * 1;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Cangkir") && wadahKonversi.equalsIgnoreCase("Gelas")) {
                volumeKonversi = volumeAwal / 2.5;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Cangkir") && wadahKonversi.equalsIgnoreCase("Botol")) {
                volumeKonversi = volumeAwal / 5;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            else if (wadahAwal.equalsIgnoreCase("Cangkir") && wadahKonversi.equalsIgnoreCase("Teko")) {
                volumeKonversi = volumeAwal / 25;
                System.out.print(volumeAwal + " " + wadahAwal + " = " + volumeKonversi + " " + wadahKonversi + "\n");
                flag = false;
            }
            // Konversi tidak ditemukan
            else {
                System.out.println("Maaf, Konversi belum ditemukan!!!!!");
                flag = true;
            }
        }
    }
}
