package ProblemSolving14;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        double[] jarakToko = {0, 0.5, 2, 3.5, 5};
        String rute;
        double jarakTempuh = 0, selisihJarak;
        int totalWaktu;

        System.out.print("Input: ");
        rute = input.nextLine();

        String[] convertRuteToString = rute.split("-");
        int[] intArray = new int[convertRuteToString.length];

        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = Integer.parseInt(convertRuteToString[i]);
        }

        for (int i = 0; i < intArray.length; i++) {
            if (i == 0) {
                jarakTempuh = jarakToko[intArray[i]];
            }
            else if (i == intArray.length - 1) {
                selisihJarak = jarakToko[intArray[i]] - jarakToko[intArray[i - 1]];

                if (selisihJarak < 0){
                    selisihJarak *= -1;
                }

                jarakTempuh += (jarakToko[intArray[i]] + selisihJarak);
            }
            else {
                selisihJarak = jarakToko[intArray[i]] - jarakToko[intArray[i - 1]];

                if (selisihJarak < 0){
                    selisihJarak *= -1;
                }

                jarakTempuh += selisihJarak;
            }
        }

        totalWaktu = (int) ((convertRuteToString.length * 10) + (jarakTempuh * 2));

        System.out.print("Jarak tempuh: " + jarakTempuh + "\n");
        System.out.print("Output: " + totalWaktu + " menit" + "\n");
    }
}
