package ProblemSolving14;

import java.util.*;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int jumlahUang, rekomendasiKacamata, rekomendasiBaju, itemRekomendasi, lastValue;
        String hargaKacamata, hargaBaju;
        ArrayList<Integer> rekomendasi = new ArrayList<>();

        System.out.print("Masukan Jumlah Uang: ");
        jumlahUang = input.nextInt();

        input.nextLine();
        System.out.print("Harga Kacamata: ");
        hargaKacamata = input.nextLine();

        System.out.print("Harga Baju: ");
        hargaBaju = input.nextLine();

        String[] textKacamata = hargaKacamata.split(", ");
        String[] textBaju = hargaBaju.split(", ");

        int[] kacamatas = new int[textKacamata.length];
        int[] bajus = new int[textBaju.length];

        for (int i = 0; i < kacamatas.length; i++) {
            kacamatas[i] = Integer.parseInt(textKacamata[i]);
        }

        for (int i = 0; i < bajus.length; i++) {
            bajus[i] = Integer.parseInt(textBaju[i]);
        }

        for (int i = 0; i < kacamatas.length; i++) {
            rekomendasiKacamata = kacamatas[i];
            for (int j = 0; j < bajus.length; j++) {
                rekomendasiBaju = bajus[j];
                itemRekomendasi = rekomendasiKacamata + rekomendasiBaju;

                if (itemRekomendasi <= jumlahUang){
                    rekomendasi.add(itemRekomendasi);
                }
            }
        }

        Collections.sort(rekomendasi);

        lastValue = rekomendasi.size() - 1;

        System.out.println("Output: " + rekomendasi.get(lastValue));
    }
}
