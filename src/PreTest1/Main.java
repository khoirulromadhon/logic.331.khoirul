package PreTest1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 10)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 10){
                System.out.println("Angka tidak tersedia !!!!!");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1:
                    Soal01.Resolve();
                    break;
                case 2:
                    Soal02.Resolve();
                    break;
                case 3:
                    Soal03.Resolve();
                    break;
                case 4:
                    Soal04.Resolve(); // belum selesai
                    break;
                case 5:
                    Soal05.Resolve();
                    break;
                case 6:
                    Soal06.Resolve();
                    break;
                case 7:

                    break;
                case 8:
                    Soal08.Resolve();
                    break;
                case 9:
                    Soal09.Resolve();
                    break;
                case 10:
                    Soal10.Resolve();
                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}