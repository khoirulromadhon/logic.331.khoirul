package PreTest1;

import java.util.Scanner;

public class Soal05 {
    public static void  Resolve(){
        Scanner input = new Scanner(System.in);
        int porsiLaki = 0, porsiPerempuan = 0, porsiBalita = 0, inputLaki, inputPerempuan, inputBalita, pilihan, totalPorsiAwal, totalPorsiAkhir;
        char confirm;
        boolean flagInput = true;

        while (flagInput) {
            System.out.println("Pilih customer");
            System.out.println("1. Laki-laki");
            System.out.println("2. Perempuan");
            System.out.println("3. Balita");
            System.out.print("pilihan: ");
            pilihan = input.nextInt();

            if (pilihan == 1) {
                System.out.print("Porsi: ");
                inputLaki = input.nextInt();
                porsiLaki += inputLaki;

                System.out.println("Apakah anda ingin memasukan data lagi ? (y/n)");
                confirm = input.next().charAt(0);

                if (confirm != 'y'){
                    flagInput = false;
                }
                else {
                    flagInput = true;
                }
            }
            else if (pilihan == 2) {
                System.out.print("Porsi: ");
                inputPerempuan = input.nextInt();
                porsiPerempuan += inputPerempuan;

                System.out.println("Apakah anda ingin memasukan data lagi ? (y/n)");
                confirm = input.next().charAt(0);

                if (confirm != 'y'){
                    flagInput = false;
                }
                else {
                    flagInput = true;
                }
            }
            else if (pilihan == 3) {
                System.out.print("Porsi: ");
                inputBalita = input.nextInt();
                porsiBalita += inputBalita;

                System.out.println("Apakah anda ingin memasukan data lagi ? (y/n)");
                confirm = input.next().charAt(0);

                if (confirm != 'y'){
                    flagInput = false;
                }
                else {
                    flagInput = true;
                }
            }
            else {
                System.out.println("Pilihan tidak tersedia!!!!!");
                flagInput = true;
            }
        }

        totalPorsiAwal = porsiLaki + porsiPerempuan + porsiBalita;

        if (totalPorsiAwal > 5 && totalPorsiAwal % 2 != 0){
            porsiPerempuan *= 2;

            totalPorsiAkhir = porsiLaki + porsiPerempuan + porsiBalita;
        }
        else {
            totalPorsiAkhir = totalPorsiAwal;
        }

        System.out.println(totalPorsiAkhir + " porsi");
    }
}
