package PreTest1;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int n, helper = 1, helperFibonanci = 1;
        boolean findPrima = true;

        System.out.print("Input panjang deret: ");
        n = input.nextInt();

        int[] bilanganPrima = new int[n];
        int[] bilanganFibonanci = new int[n];
        int[] outputs = new int[n];

        for (int i = 0; i < n; i++) {
            while (findPrima == true){
                if (helper == 1){
                    helper++;
                }
                else if (helper == 2) {
                    bilanganPrima[i] = helper;
                    helper++;
                    findPrima =false;
                }
                else if (helper == 3) {
                    bilanganPrima[i] = helper;
                    helper++;
                    findPrima = false;
                }
                else if (helper % 2 != 0 && helper % 3 != 0) {
                    bilanganPrima[i] = helper;
                    helper++;
                    findPrima = false;
                }
                else {
                    helper++;
                }
            }
            findPrima = true;
        }

        for (int i = 0; i < n; i++) {
            if (i == 0 || i == 1){
                bilanganFibonanci[i] = helperFibonanci;
            }
            else {
                bilanganFibonanci[i] = bilanganFibonanci[i - 1] + bilanganFibonanci[i - 2];
            }
        }

        for (int i = 0; i < n; i++) {
            outputs[i] = bilanganFibonanci[i] + bilanganPrima[i];
        }

        System.out.print("Output: ");
        for (int o: outputs) {
            System.out.print(o + " ");
        }
        System.out.println();
    }
}
