package PreTest1;

import java.util.Scanner;

public class Soal03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int n, x = 1, start = 100;

        System.out.print("Masukan n: ");
        n = input.nextInt();

        int[] ouputs = new int[n];

        for (int i = 0; i < n; i++) {
            if (i == 0){
                ouputs[i] = start;
            }
            else {
                start = 100;
                start += (x * 3);
                ouputs[i] = start;
                x += 2;
            }
        }

        for (int o : ouputs) {
            System.out.println(o + " adalah Si Angka 1");
        }
    }
}
