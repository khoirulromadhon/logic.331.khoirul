package PreTest1;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int n, ganjil = 1, genap = 2;

        System.out.print("Masukan angka: ");
        n = input.nextInt();

        while (ganjil <= n){
            System.out.print(ganjil + " ");
            ganjil+=2;
        }

        System.out.println();

        while (genap <= n){
            System.out.print(genap + " ");
            genap+=2;
        }
        System.out.println();
    }
}
