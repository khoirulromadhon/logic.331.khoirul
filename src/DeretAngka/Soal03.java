package DeretAngka;

public class Soal03 {
    public static void Resolve(int n){
        int helper = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper += 3;
        }

        Utility.PrintArray1D(results);
    }
}
