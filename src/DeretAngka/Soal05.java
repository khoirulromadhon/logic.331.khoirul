package DeretAngka;

public class Soal05 {
    public static void Resolve(int n){
        int helper = 1, count = 0;
        String[] results = new String[n];

        for (int i = 0; i < n; i++) {
            if (count == 2){
                results[i] = "*";
                count -= 2;
            }
            else {
                results[i] = Integer.toString(helper);
                helper += 4;
                count++;
            }
        }

        Utility.PrintString(results);
    }
}
