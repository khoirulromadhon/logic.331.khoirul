package DeretAngka;

public class Soal08 {
    public static void Resolve(int n){
        int helper = 3;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;;
            helper *= 3;
        }

        Utility.PrintArray1D(results);
    }
}
