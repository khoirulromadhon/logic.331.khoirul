package DeretAngka;

public class Soal11 {
    public static void Resolve(int n){
        int helper = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            if (i == 0 || i == 1){
                results[i] = helper;
            }
            else {
                results[i] = results[i-1] + results[i-2];
            }
        }
        // yang akan datang = bilangan sekarang + bilangan sebelumnya
        Utility.PrintArray1D(results);
    }
}
