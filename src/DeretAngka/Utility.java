package DeretAngka;

public class Utility {
    public static void PrintArray1D(int[] results){
        for (int i = 0; i < results.length; i++) {
            System.out.print(results[i] + " ");
        }
        System.out.println();
    }

    public static void PrintBintang(int[] results){
        for (int i = 0; i < results.length; i++) {
            if (results[i] % 3 == 0){
                System.out.print(" * ");
            }
            else {
                System.out.print(results[i] + " ");
            }
        }
        System.out.println();
    }

    public static void  PrintString(String[] results){
        for (int i = 0; i < results.length; i++) {
            System.out.print(results[i] + " ");
        }
        System.out.println();
    }

    public static void PrintXXX(int[] results){
        int count = 0;
        for (int i = 0; i < results.length; i++) {
            if (count == 3){
                System.out.print("xxx");
                count -= 4;
            }
            else {
                System.out.print(results[i] + " ");
                count++;
            }
        }
        System.out.println();
    }
}
