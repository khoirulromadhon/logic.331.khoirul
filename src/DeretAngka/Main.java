package DeretAngka;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan, n;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 12)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12){
                System.out.println("Angka tidak tersedia !!!!!");
                pilihan = input.nextInt();
            }

            System.out.print("Masukan nilai n: ");
            n = input.nextInt();

            switch (pilihan){
                case 1:
                    Soal01.Resolve(n);
                    break;
                case 2:
                    Soal02.Resolve(n);
                    break;
                case 3:
                    Soal03.Resolve(n);
                    break;
                case 4:
                    Soal04.Resolve(n);
                    break;
                case 5:
                    Soal05.Resolve(n);
                    break;
                case 6:
                    Soal06.Resolve(n);
                    break;
                case 7:
                    Soal07.Resolve(n);
                    break;
                case 8:
                    Soal08.Resolve(n);
                    break;
                case 9:
                    Soal09.Resolve(n);
                    break;
                case 10:
                    Soal10.Resolve(n);
                    break;
                case 11:
                    Soal11.Resolve(n);
                    break;
                case 12:
                    Soal12.Resolve(n);
                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}