package DeretAngka;

public class Soal02 {
    public static void Resolve(int n){
        int helper = 2;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper += 2;
        }

        Utility.PrintArray1D(results);
    }
}
