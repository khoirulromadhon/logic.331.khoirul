package WarmUp;
import java.util.Scanner;

public class PlusMinus {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int jumlahAngka;

        System.out.print("Masukan jumlah angka: ");
        jumlahAngka = input.nextInt();

        int[] intArray = new int[jumlahAngka];

        for (int i = 0; i < intArray.length; i++) {
            System.out.print("Input " + (i + 1) + ": ");
            intArray[i] = input.nextInt();
        }

        calculatePercentage(intArray);
    }

    static void calculatePercentage(int[] intArray) {
        int totalNumbers = intArray.length;
        int positiveCount = 0;
        int negativeCount = 0;
        int zeroCount = 0;

        for (int num : intArray) {
            if (num > 0) {
                positiveCount++;
            } else if (num < 0) {
                negativeCount++;
            } else {
                zeroCount++;
            }
        }

        double positivePercentage = (double) positiveCount / totalNumbers * 100;
        double negativePercentage = (double) negativeCount / totalNumbers * 100;
        double zeroPercentage = (double) zeroCount / totalNumbers * 100;

        System.out.printf("Positive = %.1f %%\n", positivePercentage);
        System.out.printf("Negative = %.1f %%\n", negativePercentage);
        System.out.printf("Zero = %.1f %%\n", zeroPercentage);
    }
}
