package WarmUp;

import java.util.Scanner;

public class SimpleArraySum {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int length, total = 0;

        System.out.print("Masukan banyaknya angka: ");
        length = input.nextInt();

        int[] numbers = new int[length];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print("Masukan angka " + (i + 1) + ": ");
            numbers[i] = input.nextInt();
        }

        for (int n :numbers) {
            total += n;
        }

        System.out.println("Total: " + total);
    }
}
