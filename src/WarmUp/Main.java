package WarmUp;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan, n;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 10)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12){
                System.out.println("Angka tidak tersedia !!!!!");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1:
                    SolveMeFirst.Resolve();
                    break;
                case 2:
                    TimeConversion.Resolve();
                    break;
                case 3:
                    SimpleArraySum.Resolve();
                    break;
                case 4:
                    DiagonalDifference.Resolve();
                    break;
                case 5:
                    PlusMinus.Resolve();
                    break;
                case 6:
                    StairCase.Resolve();
                    break;
                case 7:
                    MinMaxSum.Resolve();
                    break;
                case 8:
                    BirtdayCakeCandles.Resolve();
                    break;
                case 9:
                    AVeryBigSum.Resolve();
                    break;
                case 10:
                    CompareTheTriplets.Resolve();
                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}