package WarmUp;

import java.util.Scanner;

public class BirtdayCakeCandles {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int candle, max = 0, countMax = 0;

        System.out.print("Masukan banyaknya lilin: ");
        candle = input.nextInt();

        int[] candles = new int[candle];

        for (int i = 0; i < candles.length; i++) {
            System.out.print("Masukan lilin ke " + (i + 1) + ": ");
            candles[i] = input.nextInt();
        }

        for (int i = 0; i < candles.length; i++) {
            if (candles[i] > max){
                max = candles[i];
            }
        }

        for (int i = 0; i < candles.length; i++) {
            if (candles[i] == max){
                countMax++;
            }
        }

        System.out.println("Output: " + countMax);
    }
}
