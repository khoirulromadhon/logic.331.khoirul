package WarmUp;

import java.util.Scanner;

public class AVeryBigSum {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int n;
        long total = 0;

        System.out.print("Masukan banyaknya angka: ");
        n = input.nextInt();

        long[] numbers = new long[n];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print("Value" + (i + 1) + ": ");
            numbers[i] = input.nextLong();
        }

        for (int i = 0; i < numbers.length; i++) {
            total += numbers[i];
        }

        System.out.println("Output: " + total);
    }
}
