package InputPlural;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);

        calculatePercentage(intArray);
    }

    static void calculatePercentage(int[] intArray) {
        int totalNumbers = intArray.length;
        int positiveCount = 0;
        int negativeCount = 0;
        int zeroCount = 0;

        for (int num : intArray) {
            if (num > 0) {
                positiveCount++;
            } else if (num < 0) {
                negativeCount++;
            } else {
                zeroCount++;
            }
        }

        double positivePercentage = (double) positiveCount / totalNumbers * 100;
        double negativePercentage = (double) negativeCount / totalNumbers * 100;
        double zeroPercentage = (double) zeroCount / totalNumbers * 100;

        System.out.printf("Positive = %.1f %%\n", positivePercentage);
        System.out.printf("Negative = %.1f %%\n", negativePercentage);
        System.out.printf("Zero = %.1f %%\n", zeroPercentage);
    }
}
