package InputPlural;

import java.util.Arrays;
import java.util.Scanner;

public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);

        calculateMinMaxSum(intArray);
    }

    static void calculateMinMaxSum(int[] intArray) {
        Arrays.sort(intArray);

        int minSum = 0;
        int maxSum = 0;

        for (int i = 0; i < intArray.length - 1; i++) {
            minSum += intArray[i];
            maxSum += intArray[i + 1];
        }

        System.out.println("Terkecil: " + minSum);
        System.out.println("Terbesar: " + maxSum);
    }
}
