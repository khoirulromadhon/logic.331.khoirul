package InputPlural;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length, helper = 0, pembanding = 0, number = 0;

        for (int i = 0; i < length; i++) {
            for (int j = 1; j < length; j++) {
                pembanding = intArray[j - 1];
                number = intArray[j];

                if (number < pembanding){
                    helper = pembanding;
                    intArray[j - 1] = number;
                    intArray[j] = helper;
                }
            }
        }

        System.out.print("Output: ");
        for (int i = 0; i < length; i++) {
            System.out.print(intArray[i] + " ");
        }
        System.out.println();
    }
}
