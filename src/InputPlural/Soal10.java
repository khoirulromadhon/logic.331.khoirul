package InputPlural;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        System.out.print("Output: ");

        for (int i = 0; i < length; i++) {
            if (intArray[i] == 2){
                System.out.print(intArray[i] + ", ");
            }
            else if (intArray[i] == 3) {
                System.out.print(intArray[i] + ", ");
            }
            else if (intArray[i] % 2 != 0 && intArray[i] % 3 != 0) {
                System.out.print(intArray[i] + ", ");
            }
        }

        System.out.println(" adalah bilangan prima");
    }
}
