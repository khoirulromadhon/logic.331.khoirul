package InputPlural;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Soal06 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length, couple = 0;

        int count = countPairs(intArray);
        System.out.println("Output: " + count);
    }

    static int countPairs(int[] intArray) {
        Set<Integer> set = new HashSet<>();
        int count = 0;

        for (int num : intArray) {
            if (set.contains(num)) {
                count++;
            } else {
                set.add(num);
            }
        }

        return count;
    }
}
