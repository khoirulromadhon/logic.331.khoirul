package InputPlural;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length, total = 0;

        for (int i = 0; i < length; i++) {
            total += intArray[i];
        }

        System.out.println("Output: " + total);
    }
}
