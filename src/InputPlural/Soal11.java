package InputPlural;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String merge = " ";
        char helper;

        System.out.print("Input: ");
        String text = input.nextLine();
        System.out.print("sort ASC / DSC: ");
        String sort = input.nextLine();

        String[] splits = text.split(" ");

        for (String str : splits) {
            merge = merge.concat(str);
        }

        char[] chars = merge.toCharArray();

        if (sort.equalsIgnoreCase("asc")){
            for (int i = 0; i < chars.length; i++) {
                for (int j = 1; j < chars.length; j++) {
                    if (chars[j] < chars[j - 1]){
                        helper = chars[j - 1];
                        chars[j - 1] = chars[j];
                        chars[j] = helper;
                    }
                }
            }
        }
        else if (sort.equalsIgnoreCase("dsc")) {
            for (int i = 0; i < chars.length; i++) {
                for (int j = 1; j < chars.length; j++) {
                    if (chars[j] > chars[j - 1]){
                        helper = chars[j - 1];
                        chars[j - 1] = chars[j];
                        chars[j] = helper;
                    }
                }
            }
        }
        else {
            System.out.println("Pilihanya hanya asc & dsc, jangan mengada ngada!!!");
        }

        System.out.print("Output: ");
        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i] + " ");
        }
        System.out.println();
    }
}
