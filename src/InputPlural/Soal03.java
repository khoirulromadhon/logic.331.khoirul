package InputPlural;

import java.util.Scanner;

public class Soal03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        Double[] doubleArray = Utility.ConvertStringToArrayDouble(text);
        int length = doubleArray.length, midPosition;
        double midValue = 0;

        midPosition = length / 2;

        if (length % 2 != 0){
            for (int i = 0; i < length; i++) {
                if (i == midPosition){
                    midValue = doubleArray[i];
                }
            }
        }
        else if (length % 2 == 0) {
            for (int i = 0; i < length; i++) {
                if (i == midPosition){
                    midValue = (doubleArray[i] + doubleArray[i - 1]) / 2;
                }
            }
        }


        System.out.println("Output: " + midValue);
    }
}
