package InputPlural;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length, max = 0, count = 0;

        for (int i = 1; i < length; i++) {
           if (intArray[i] > max){
               max = intArray[i];
           }
        }

        for (int i = 0; i < length; i++) {
            if (intArray[i] == max){
                count++;
            }
        }

        System.out.println("Output: " + count);
    }
}
