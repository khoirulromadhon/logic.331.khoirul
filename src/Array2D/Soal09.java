package Array2D;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan n: ");
        int n = input.nextInt();

        int[][] array2D = new int[3][n];
        int baris1 = 0, baris2 = 0, n2 = 3;



        for (int i = 0; i < 3; i++) {
            int baris3 = baris2 - n2;
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    array2D[i][j] = baris1;
                    baris1++;
                }
                else if (i == 1) {
                    array2D[i][j] = baris2;
                    baris2 += 3;
                }
                else if (i == 2) {
                    array2D[i][j] = baris3;
                    baris3 -= 3;
                }
            }
        }
        Utility.PrintArray2D(array2D);
    }
}
