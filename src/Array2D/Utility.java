package Array2D;

public class Utility {
    public static void PrintArray2D(int[][] array2D){
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[0].length; j++) {
                System.out.print(array2D[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void PintSoal02(int[][] array2D){
        int count = 0;

        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[0].length; j++) {
                if (i == 0) {
                    System.out.print(array2D[i][j] + " ");
                }
                else if (i == 1){
                    if (count == 2){
                        System.out.print("-" + array2D[i][j] + " ");
                        count -= 2;
                    }
                    else {
                        System.out.print(array2D[i][j] + " ");
                        count++;
                    }
                }
            }
            System.out.println();
        }
    }

    public static void PrintSoal07(int[][] array2D){
        int count = 0;

        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[0].length; j++) {
                if (i == 0){
                    if (count == 3){
                        System.out.print("-" + array2D[i][j] + " ");
                        count -= 3;
                    }
                    else {
                        System.out.print(array2D[i][j] + " ");
                        count++;
                    }
                }
                else if (i == 1) {
                    if (count == 2){
                        System.out.print("-" + array2D[i][j] + " ");
                        count -= 2;
                    }
                    else {
                        System.out.print(array2D[i][j] + " ");
                        count++;
                    }
                }
                else if (i == 2) {
                    if (count == 2){
                        System.out.print("-" + array2D[i][j] + " ");
                        count -= 2;
                    }
                    else {
                        System.out.print(array2D[i][j] + " ");
                        count++;
                    }
                }
            }
            if (i == 1){
                count = 1;
            }
            else {
                count = 0;
            }
            System.out.println();
        }
    }

    public static void PrintSoal11(String[][] star) {
        int length = star.length;
        int batas = length - 2;

        System.out.println("Length = " + length);
        System.out.println("Batas = " + batas);

        for (int i = 0; i < star.length; i++) {
            for (int j = 0; j < star[0].length; j++) {
                if (j > batas){
                    System.out.print(star[i][j]);
                    batas--;
                }
                else {
                    batas--;
                }
            }
            System.out.println();
        }
    }
}
