package Array2D;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan n: ");
        int n = input.nextInt();

        int rows = n; // Jumlah baris

        for (int i = 1; i <= rows; i++) {
            // Cetak spasi untuk setiap baris (baris ke-1 memiliki 6 spasi, baris ke-2 memiliki 5 spasi, dan seterusnya)
            for (int j = 1; j <= rows - i; j++) {
                System.out.print("	"); // Tab karakter (atau Anda bisa gunakan spasi)
            }

            // Cetak bintang untuk setiap baris (baris ke-1 memiliki 1 bintang, baris ke-2 memiliki 2 bintang, dan seterusnya)
            for (int k = 1; k <= i; k++) {
                System.out.print("*	");
            }

            System.out.println(); // Pindah ke baris berikutnya setelah mencetak bintang
        }
    }
}
