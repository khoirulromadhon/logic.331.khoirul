package Array2D;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan n: ");
        int n = input.nextInt();

        int[][] array2D = new int[2][n];
        int baris1 = 0, baris2 = 1, n2 = 5;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    array2D[i][j] = baris1;
                    baris1++;
                }
                else if (i == 1) {
                    if (j % 2 == 0){
                        array2D[i][j] = baris2;
                        baris2++;
                    }
                    else {
                        array2D[i][j] = n2;
                        n2 += 5;
                    }
                }
            }
        }
        Utility.PrintArray2D(array2D);
    }
}
