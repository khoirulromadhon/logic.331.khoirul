package Array2D;

import java.util.Scanner;

public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan n: ");
        int n = input.nextInt();

        int[][] array2D = new int[3][n];
        int start = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                array2D[i][j] = start;
                start++;
            }
        }
        Utility.PrintSoal07(array2D);
    }
}
