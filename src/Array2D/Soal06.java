package Array2D;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan n: ");
        int n = input.nextInt();

        int[][] array2D = new int[3][n];
        int baris1 = 0, kecuali1 = 1;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    array2D[i][j] = baris1;
                    baris1++;
                }
                else if (i == 1) {
                    array2D[i][j] = kecuali1;
                    kecuali1 *= n;
                }
                else if (i == 2) {
                    array2D[i][j] = array2D[i-2][j] + array2D[i-1][j];
                }
            }
        }

        Utility.PrintArray2D(array2D);
    }
}
