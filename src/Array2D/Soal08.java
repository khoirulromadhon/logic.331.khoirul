package Array2D;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan n: ");
        int n = input.nextInt();

        int[][] array2D = new int[3][n];
        int start = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    array2D[i][j] = start;
                    start++;
                }
                else if (i == 1) {
                    array2D[i][j] = start;
                    start += 2;
                }
                else if (i == 2) {
                    array2D[i][j] = start;
                    start += 3;
                }
            }
            start = 0;
        }
        Utility.PrintArray2D(array2D);
    }
}
