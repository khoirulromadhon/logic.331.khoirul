// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String xsis = "XSIS Mitra Utama";

        String[] xsisSplits = xsis.split(" ");

        System.out.println(xsis);

        for (int i = 0; i < xsisSplits.length; i++) {
            char[] hurufs = xsisSplits[i].toCharArray();
            for (int j = 0; j < hurufs.length; j++) {
                if (j == 0 || j == (hurufs.length - 1)){
                    System.out.print(hurufs[j]);
                }
                else {
                    System.out.print("*");
                }
            }
            System.out.print(" ");
        }
    }
}