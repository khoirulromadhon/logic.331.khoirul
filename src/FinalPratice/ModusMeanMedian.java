package FinalPratice;

import java.util.ArrayList;
import java.util.Scanner;

public class ModusMeanMedian {
    public static void Resolve(){
//        int[] derets = {8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3};
        Scanner input = new Scanner(System.in);
        String deret;
        int modus, mid, find;
        double a, b, median, mean = 0, total = 0;

        System.out.print("Masuka deret: ");
        deret = input.nextLine();

        String[] derets = deret.split(", ");
        int[] intDerets = new int[derets.length];

        for (int i = 0; i < intDerets.length; i++) {
            intDerets[i] = Integer.parseInt(derets[i]);
        }

        // Modus


        // Mean
        for (int i = 0; i < intDerets.length; i++) {
            total += (double) (intDerets[i]);
            mean = total / intDerets.length;
        }

        // Median
        if (intDerets.length % 2 == 0){
            mid = intDerets.length / 2;
            a = (double) (intDerets[mid]);
            b = (double) (intDerets[mid - 1]);
            median = (a + b) / 2;
        }
        else {
            mid = intDerets.length / 2;
            median = intDerets[mid];
        }

        System.out.println("Modus: " );
        System.out.println("Mean: " + mean);
        System.out.println("Median: " + median);
    }
}
