package FinalPratice;

import java.util.Scanner;

public class RotasiDeret {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int[] numbers = {1, 2, 3, 4, 5};
        int rotasi, helper;

        System.out.println("Deret angka");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
        System.out.print("Anda ingin melakukan berapa kali rotasi: ");
        rotasi = input.nextInt();

        for (int i = 0; i < rotasi; i++) {
            helper = numbers[0];

            for (int j = 0; j < numbers.length; j++) {
                if (j == numbers.length - 1){
                    numbers[j] = helper;
                }
                else {
                    numbers[j] = numbers[j + 1];
                }
            }
        }

        System.out.print("Hasil Rotasi: ");
        for (int number: numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }
}
