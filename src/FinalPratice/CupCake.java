package FinalPratice;

import java.util.Scanner;

public class CupCake {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int n;
        double terigu, gula, susu;

        System.out.print("Masukan jumlah Cup Cake yang ingin dibuat: ");
        n = input.nextInt();

        terigu = n * 8.3;
        gula = n * 6.7;
        susu = n * 6.7;

        System.out.println("Bahan yang diperlukan untuk membuat " + n +" Cup Cake adalah: ");
        System.out.println(terigu + "gr Tepung Terigu");
        System.out.println(gula + "gr Gula");
        System.out.println(susu + "mL Susu");
    }
}
