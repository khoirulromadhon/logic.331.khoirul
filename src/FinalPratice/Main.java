package FinalPratice;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 10)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 11){
                System.out.println("Angka tidak tersedia !!!!!");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1:
                    Loli.Resolve();
                    break;
                case 2:
                    Palindrome.Resolve(); // belum selesai
                    break;
                case 3:
                    RotasiDeret.Resolve();
                    break;
                case 4:

                    break;
                case 5:
                    ConvertJam.Resolve();
                    break;
                case 6:

                    break;
                case 7:
                    ModusMeanMedian.Resolve(); // kurang modus
                    break;
                case 8:

                    break;
                case 9:

                    break;
                case 10:
                    CupCake.Resolve();
                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}