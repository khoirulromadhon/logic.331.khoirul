package BankBRO;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int layanan, count = 0;
        String pinRegister, pinUser, pinLogin;
        boolean flagRegistrasi = true, flagLogin = true, flagMenu = true;
        char confirm;

        System.out.println("******************** Selamat Datang di Bank BRO ********************");

        // Registrasi
        while (flagRegistrasi){
        System.out.print("Registrasi PIN anda: ");
        pinRegister = input.nextLine();

            // Check numeric & length
            if (Utility.IsNumeric(pinRegister) == true && Utility.LengthCheck(pinRegister) == true){
                while (flagLogin){
                pinUser = pinRegister;

                // Login
                System.out.print("Masukan PIN anda: ");
                pinLogin = input.nextLine();

                    // Check Pin
                    if (pinLogin.equals(pinUser)) {
                        // Menu
                        while (flagMenu){
                            System.out.println("1. Setoran Tunai");
                            System.out.println("2. Transfer");
                            System.out.print("Pilihan: ");
                            layanan = input.nextInt();

                            // Setor Tunai
                            if (layanan == 1){
                                SetorTunai.Setor();

                                System.out.print("Apakah anda ingin melakukan transaksi lagi ? (y/n) ");
                                confirm = input.next().charAt(0);

                                if (confirm != 'y'){
                                    flagMenu = false;
                                    flagLogin = false;
                                    flagRegistrasi = false;
                                }
                            }
                            // Tranfer
                            else if (layanan == 2) {
                                TransferTunai.Transfer();

                                System.out.print("Apakah anda ingin melakukan transaksi lagi ? (y/n) ");
                                confirm = input.next().charAt(0);

                                if (confirm != 'y'){
                                    flagMenu = false;
                                    flagLogin = false;
                                    flagRegistrasi = false;
                                }
                            }
                            else {
                                System.out.println("Pilihan Tidak ada !!!!");
                            }
                        }
                    }
                    else {
                        System.out.println("PIN anda salah !!!!!");
                        flagLogin = true;
                        count++;

                        // Blokir PIN salah 3x
                        if (count == 3){
                            System.out.println("ATM anda terblokir !!!!!");
                            flagLogin = false;
                            flagRegistrasi = false;
                        }
                    }
                }
                // Stop While Login
                flagLogin = false;
            }
            // Stop While Registrasi
            else {
                flagRegistrasi = true;
            }
        }
    }
}