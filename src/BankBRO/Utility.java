package BankBRO;

public class Utility {
    // Check Apakah PIN Numerik
    public static boolean IsNumeric (String numberString){
        if (numberString == null){
            System.out.println("PIN Tidak Boleh Kosong !!!!!");
            return false;
        }
        try{
            int validation = Integer.parseInt(numberString);
        }
        catch (NumberFormatException exception){
            System.out.println("PIN harus terdiri dari angka !!!!!");
            return false;
        }
        return true;
    }

    // Check PIN length
    public static boolean LengthCheck (String numberString) {
        if (numberString.length() == 6){
            return true;
        }
        else {
            System.out.println("PIN harus 6 digit !!!!!");
            return false;
        }
    }
}
