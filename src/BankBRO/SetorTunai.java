package BankBRO;

import java.util.Scanner;

public class SetorTunai {
    private static Scanner input = new Scanner(System.in);
    private static long setoran;

    public static long saldo = 0;
    private static char confirmSetor;

    public static long Setor(){
        // Input nominal setor
        System.out.print("Masukan nominal setor: ");
        setoran = input.nextLong();

        // Konfirmasi nominal
        System.out.print("Are you sure ? (y/n) ");
        confirmSetor = input.next().charAt(0);

        // Check limit setoran
        while (setoran >= 25000000){
            System.out.println("Nominal melebihi limit !!!!!");

            System.out.print("Masukan nominal setor: ");
            setoran = input.nextLong();

            System.out.print("Are you sure ? (y/n) ");
            confirmSetor = input.next().charAt(0);

            saldo += setoran;
        }

        // Menyimpan value setoran
        saldo += setoran;
        System.out.println("Setor Berhasil");
        System.out.print("Saldo anda: " + saldo + "\n");

        return saldo;
    }
}
