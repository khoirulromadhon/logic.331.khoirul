package BankBRO;

import java.util.Scanner;

public class TransferTunai {
    private static Scanner input = new Scanner(System.in);
    private static int  pilihan;

    private static String rekening;
    private static long nominalTransfer, saldo = SetorTunai.saldo;

    public static long Transfer(){
        System.out.println("1. Antar Rekening");
        System.out.println("2. Antar Bank");
        System.out.print("Pilihan: ");
        pilihan = input.nextInt();

        if (pilihan == 1){
            System.out.println("Masukan rekening penerima: ");
            rekening = input.nextLine();

            while (rekening.length() != 7){
                System.out.println("Masukan rekening penerima: ");
                rekening = input.nextLine();
            }

            System.out.print("Masukan nominal transfer: ");
            nominalTransfer = input.nextLong();

            if (nominalTransfer > saldo){
                System.out.println("Saldo anda Tidak cukup !!!!!");
            }
            else {
                saldo -= nominalTransfer;
                System.out.println("Transaksi Berhasil !!!!!!");
                System.out.println("Sisa saldo anda " + saldo);

                return saldo;
            }
        }
        else if (pilihan == 2) {
            System.out.println("Masukan rekening penerima: ");
            rekening = input.nextLine();

            while (rekening.length() != 10){
                System.out.println("Masukan rekening penerima: ");
                rekening = input.nextLine();
            }

            System.out.print("Masukan nominal transfer: ");
            nominalTransfer = input.nextLong();

            if (nominalTransfer > saldo){
                System.out.println("Saldo anda Tidak cukup !!!!!");
            }
            else {
                saldo -= nominalTransfer;
                System.out.println("Transaksi Berhasil !!!!!!");
                System.out.println("Sisa saldo anda " + saldo);

                return saldo;
            }
        }
        else {
            System.out.println("Pilihan tidak tersedia !!!!!");
        }

        return saldo;
    }
}
